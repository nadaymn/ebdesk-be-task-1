import java.util.Scanner;

public class main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Masukan panjang fibonacci yang diinginkan : ");
        int n = in.nextInt();

        if (n == 1) {
            System.out.println("\nFibonacci ke - 1 adalah 0");
        } else {
            int[] fibonacci = new int[n + 2];
            fibonacci[0] = 0;
            fibonacci[1] = 1;

            for (int i = 0;i < fibonacci.length; i++) {
                try {
                    fibonacci[i + 2] = fibonacci[i] + (int) Math.pow(fibonacci[i + 1], 2);
                    System.out.println("\nFibonacci ke - " + (i + 1) + " adalah " + fibonacci[i]);
                } catch (ArrayIndexOutOfBoundsException error) {
                    System.out.println(' ');
                }
            }
        }

    }

}
